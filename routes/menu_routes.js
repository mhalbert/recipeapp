var express = require('express');
var router = express.Router();
var menu_dal = require('../model/menu_dal');
var recipe_dal = require('../model/recipe_dal');

// View All menus
router.get('/all', function(req, res) {
    menu_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('menu/menuViewAll', { 'result':result });
        }
    });

});

router.get('/', function(req, res){
    if(req.query.menu_id == null) {
        res.send('menu_id is null');
    }
    else {
        menu_dal.getById(req.query.menu_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('menu/menuViewById', {'result': result });
            }
        });
    }
});


router.get('/edit', function(req, res){
    if(req.query.menu_id == null ) {
        res.send('A menu id and account id are required');
    }
    else {
        menu_dal.getById(req.query.menu_id, function(err, menu) {
            if (err) {
                res.send(err);
            }
            else {
                recipe_dal.getAll(function(err, allrecipe) {
                    res.render('menu/menuUpdate', {'menu': menu, 'recipeall': allrecipe });
                });
            }
        });
    }

});

router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    recipe_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('menu/menuAdd', {'recipe': result});
        }
    });
});

router.post('/insert', function(req, res){
    // simple validation
    if(req.body.menu_name.trim() == "") {
        res.send('Menu Name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        menu_dal.insert(req.body, function(err,resume_id) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                menu_dal.getAll(function(err, result) {
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('menu/menuViewAll', { 'result':result,
                            'menu_name': req.body.menu_name,
                            'what': "added",
                            'was_successful': true });
                    }
                });

            }
        });
    }
});

router.post('/update', function(req, res) {
    if(req.body.menu_name.trim() == "") {
        res.send('Menu Name must be provided.');
    }
    else {
        menu_dal.update(req.body, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                menu_dal.getAll(function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('menu/menuViewAll', {
                            'result': result,
                            'menu_name': req.body.menu_name,
                            'what': "updated",
                            'was_successful': true
                        });
                    }
                });
            }
        });
    }
});

router.get('/delete', function(req, res){
    if(req.query.menu_id == null) {
        res.send('menu_id is null');
    }
    else {
        menu_dal.delete(req.query.menu_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                menu_dal.getAll(function(err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('menu/menuViewAll', {
                            'result': result,
                            'menu_name': req.query.menu_name,
                            'what': "deleted",
                            'was_successful': true
                        });
                    }
                });
            }
        });
    }
});


module.exports = router;