var express = require('express');
var router = express.Router();
var recipe_dal = require('../model/recipe_dal');
var type_dal = require('../model/type_dal');

router.get('/about', function(req, res) {
    res.render('recipe/recipeAbout');

});

// View All recipes
router.get('/all', function(req, res) {
    recipe_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('recipe/recipeViewAll', { 'result':result });
        }
    });

});

// View the recipe for the given id
router.get('/', function(req, res){
    if(req.query.recipe_id == null) {
        res.send('recipe_id is null');
    }
    else {
        recipe_dal.getById(req.query.recipe_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('recipe/recipeViewById', {
                    'recipe': result[0][0], 'ingredient': result[1], 'type': result[2]
                });
            }
        });
    }
});

router.get('/searchAll', function(req, res){
    type_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('recipe/recipeSearch', {'type': result});
        }
    });
});

router.get('/search', function(req, res){
    recipe_dal.getByFilter(req.query, function(err, result) {
        if (err){
            res.send(err);
        }
        else {
            res.render('recipe/recipeSearchResult', {'result': result});
        }
    });
});

router.get('/searchLess', function(req, res){
    recipe_dal.getLess(req.query.num_ingredient, function(err, result) {
        if (err){
            res.send("Failed in getting recipe");
        }
        else {
            res.render('recipe/recipeSearchResult', {'result': result});
        }
    });
});

router.get('/searchNoIngredient', function(req, res){
    recipe_dal.getNoIngredient(req.query.no_ingredient, function(err, result) {
        if (err){
            res.send("Failed in getting recipe without certain ingredient");
        }
        else {
            res.render('recipe/recipeSearchResult', {'result': result});
        }
    });
});

router.get('/edit', function(req, res){
    if(req.query.recipe_id == null ) {
        res.send('A recipe id and account id are required');
    }
    else {
        recipe_dal.getById(req.query.recipe_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                type_dal.getAll(function(err, alltype) {
                    res.render('recipe/recipeUpdate', {
                        'recipe': result[0][0], 'ingredient': result[1], 'type': result[2], 'typeall': alltype
                    });
                });
            }
        });
    }

});

router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    type_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('recipe/recipeAdd', {'type': result});
        }
    });
});

router.post('/insert', function(req, res){
    // simple validation
    if(req.body.recipe_name == null) {
        res.send('Recipe Name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        recipe_dal.insert(req.body, function(err,resume_id) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                recipe_dal.getAll(function(err, result) {
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('recipe/recipeViewAll', {
                            'result': result,
                            'recipe_name': req.body.recipe_name,
                            'what': "added",
                            'was_successful': true
                        });
                    }
                });

            }
        });
    }
});

router.post('/update', function(req, res) {
    recipe_dal.update(req.body, function(err, result){
        if (err) {
            res.send(err);
        }
        else {
            recipe_dal.getAll(function(err, result) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('recipe/recipeViewAll', {
                        'result': result,
                        'recipe_name': req.body.recipe_name,
                        'what': "updated",
                        'was_successful': true
                    });
                }
            });
        }
    });
});

router.get('/delete', function(req, res){
    if(req.query.recipe_id == null) {
        res.send('recipe_id is null');
    }
    else {
        recipe_dal.delete(req.query.recipe_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                recipe_dal.getAll(function(err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('recipe/recipeViewAll', {
                            'result': result,
                            'recipe_name': req.query.recipe_name,
                            'what': "deleted",
                            'was_successful': true});
                    }
                });
            }
        });
    }
});

module.exports = router;