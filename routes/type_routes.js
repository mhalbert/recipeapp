var express = require('express');
var router = express.Router();
var type_dal = require('../model/type_dal');

// View All recipes
router.get('/all', function(req, res) {
    type_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('type/typeViewAll', { 'result':result });
        }
    });

});

router.get('/add', function(req, res) {
    res.render('type/typeAdd', {});
});

router.post('/insert', function(req, res){
    // simple validation
    if(req.body.type_name.trim() == "") {
        res.send('Type Name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        type_dal.insert(req.body, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                type_dal.getAll(function(err, result) {
                    res.render('type/typeViewAll', {
                        'result': result,
                        'type_name': req.body.type_name,
                        'what': "added",
                        'was_successful': true
                    });
                });
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if(req.query.type_id == null) {
        res.send('type_id is null');
    }
    else {
        type_dal.getById(req.query.type_id, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('type/typeUpdate', {'result': result});
            }
        });
    }
});

router.post('/update', function(req, res) {
    if(req.body.type_name.trim() == "") {
        res.send('type name must be provided');
    }
    else {
        type_dal.update(req.body, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                type_dal.getAll(function (err, result) {
                    res.render('type/typeViewAll', {
                        'result': result,
                        'type_name': req.body.type_name,
                        'what': "updated",
                        'was_successful': true
                    });
                });
            }
        });
    }
});

router.get('/delete', function(req, res) {
    if(req.query.type_id == null) {
        res.send('type_id is null');
    }
    else {
        type_dal.delete(req.query.type_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                type_dal.getAll(function(err, result) {
                    res.render('type/typeViewAll', {
                        'result': result,
                        'type_name': req.query.type_name,
                        'what': "deleted",
                        'was_successful': true
                    });
                });
            }
        });
    }
})

module.exports = router;