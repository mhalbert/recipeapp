var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join type a on a.type_id = s.type_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM type ' +
                'ORDER BY type_name ';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(type_id, callback) {
    var query = 'SELECT * FROM type ' +
        'WHERE type_id = ? ';
    var queryData = [type_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    //  INSERT THE type
    var query = 'INSERT INTO type (type_name) VALUES (?) ';

    var queryData = [params.type_name];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });

};

exports.delete = function(type_id, callback) {
    var query = 'DELETE FROM type WHERE type_id = ? ';
    var queryData = [type_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE type SET type_name = ? WHERE type_id = ?';
    var queryData = [params.type_name, params.type_id];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);

    });

};


